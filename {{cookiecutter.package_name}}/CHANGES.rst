.. -*- coding: utf-8 -*-

Changes
-------

{{cookiecutter.version}} (unreleased)
{{cookiecutter.version|count * "~"}}~~~~~~~~~~~~~

- Initial effort.
